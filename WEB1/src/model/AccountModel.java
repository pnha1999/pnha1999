package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import controller.MyConnect;
import entities.Account;

public class AccountModel {
	Account acc;
	public AccountModel() {
	}
	public AccountModel(Account acc) {
		this.acc=acc;
	}
	public ArrayList getList() {
		ArrayList<Account> list = new ArrayList<>();
		Connection cn= new MyConnect().getcn();
		if(cn==null) return null;
		try {
			String sql= "Select * from test";
			PreparedStatement ps = cn.prepareStatement(sql);
			ResultSet rs= ps.executeQuery();
			while(rs.next()) {
				Account temp = new Account(rs.getString(1), rs.getString(2));
				list.add(temp);
			}
			ps.close();
			cn.close();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public int insertAccount() {
		int kq=0;
		Connection cn= new MyConnect().getcn();
		if(cn==null) return 0;
		try {
			String sql="insert into test values(?,?)";
			PreparedStatement ps= cn.prepareStatement(sql);
			ps.setString(1, acc.getUsername());
			ps.setString(2, acc.getPassword());
			kq=ps.executeUpdate();
		}
		catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return kq;
	}
	
	public int updateAccount() {
		int kq=0;
		Connection cn= new MyConnect().getcn();
		if(cn==null) return 0;
		try {
			String sql="update test set password=? where username=?";
			PreparedStatement ps= cn.prepareStatement(sql);
			ps.setString(1, acc.getPassword());
			ps.setString(2, acc.getUsername());
			kq=ps.executeUpdate();
		}
		catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return kq;
	}
	
	public int deleteAccount() {
		int kq=0;
		Connection cn= new MyConnect().getcn();
		if(cn==null) return 0;
		try {
			String sql="delete from test where username=?";
			PreparedStatement ps= cn.prepareStatement(sql);
			ps.setString(1, acc.getUsername());
			kq=ps.executeUpdate();
			System.out.println(kq);
		}
		catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return kq;
	}
	
	public Account getListbyUser(String user) {
		Connection cn= new MyConnect().getcn();
		if(cn==null) return null;
		try {
			String sql="select * from test where username like ?";
			PreparedStatement ps=cn.prepareStatement(sql);
			ps.setString(1, user);
			ResultSet rs=ps.executeQuery();
			if(rs.next()) {
				Account temp = new Account(rs.getString(1), rs.getString(2));
				return temp;
			}
		}
		catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
}

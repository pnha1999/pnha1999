package controller;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import model.AccountModel;

/**
 * Servlet implementation class loginServlet
 */
@WebServlet("/loginServlet")
public class loginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public loginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String username = request.getParameter("txtname");
		String password = request.getParameter("txtpass");
		String page="";
		HttpSession session = request.getSession();
		
		String message="";
		
		Connection cn= (Connection) new MyConnect().getcn();
		if(cn==null) {
			message="Ket noi that bai";
			request.setAttribute("thongbao", message);
			request.getRequestDispatcher("Error.jsp").forward(request, response);
			return;
		}
		try {
			String sql="select * from test where username like ?";
			PreparedStatement ps= (PreparedStatement) cn.prepareStatement(sql);
			ps.setString(1, username);
			ResultSet rs= ps.executeQuery();
			if(rs.next()) {
				if(rs.getString(1).equals(username) && rs.getString(2).equals(password)) {
					session.setAttribute("accountList", new AccountModel().getList());
					page="View.jsp";
				}
				else {
					message = "Login that bai";
					page="Error.jsp";
				}
			}
			else {
				message = "Username ko ton tai";
				page="Error.jsp";
			}
			request.setAttribute("thongbao", message);
			request.getRequestDispatcher(page).forward(request, response);
		}
		catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		request.getRequestDispatcher(page).forward(request, response);
	}
}

package controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.jdbc.PreparedStatement;

import entities.Account;
import model.AccountModel;

/**
 * Servlet implementation class AccountServlet
 */
@WebServlet("/AccountServlet")
public class AccountServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AccountServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String page="";
		String message="";
		HttpSession session = request.getSession();
         String username= request.getParameter("username");
		 
		 String password="";
		 
		  String  chuoi = request.getParameter("task");
		  
		  
		 Connection cn= new MyConnect().getcn();
		 if(cn==null) {
			 message="Ket noi that bai";
			 request.setAttribute("thongbao", message);
			 request.getRequestDispatcher("Error.jsp").forward(request, response);
			 return;
		 }
		 if(chuoi.equals("delete")) {
			 Account account = new Account(username, password);
		     AccountModel accmodel= new AccountModel(account);
			 int kq= accmodel.deleteAccount();
			 if(kq==0) {
				 message="delete that bai";
				 page="Error.jsp";
			 }
			 else {
				 session.setAttribute("accountList", new AccountModel().getList());
				 page="View.jsp";
			 }
			 request.setAttribute("thongbao", message);
			 request.getRequestDispatcher(page).forward(request,response);
		 }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String page="";
		String message="";
		HttpSession session = request.getSession();
         String username= request.getParameter("txtname");
		 
		 String password=request.getParameter("txtpass");
		 
		  String  chuoi = request.getParameter("task");
		  
		  
		 Connection cn= new MyConnect().getcn();
		 if(cn==null) {
			 message="Ket noi that bai";
			 request.setAttribute("thongbao", message);
			 request.getRequestDispatcher("Error.jsp").forward(request, response);
			 return;
		 }
		
if(chuoi.equals("login"))
{
		 Account a= new AccountModel().getListbyUser(username);
		 
		 if(a==null) 
		 {
			    message = "Username ko ton tai";
				page="Error.jsp";
				request.setAttribute("thongbao", message);
				request.getRequestDispatcher(page).forward(request, response);
						
		 }
		 else 
		 {
		           if(password.equals(a.getPassword()))
		           {
		        	   session.setAttribute("accountList", new AccountModel().getList());
						page="View.jsp";
						request.setAttribute("thongbao", message);
						request.getRequestDispatcher(page).forward(request, response);
		           }
		 }	 
}		 

   
	 if(chuoi.equals("insert")) {	 
//		 username= request.getParameter("txtname");
//		 
//		 password=request.getParameter("txtpass");
		 
		 Account account = new Account(username, password);
			AccountModel accmodel= new AccountModel(account);
			 int kq= accmodel.insertAccount();
			 if(kq==0) {
				 message="insert that bai";
				 page="Error.jsp";
			 }
			 else {
				 session.setAttribute("accountList", new AccountModel().getList());
				 page="View.jsp";
			 }
			 request.setAttribute("thongbao", message);
			 request.getRequestDispatcher(page).forward(request,response);
		 }
	 
	 if(chuoi.equals("update")) {
		 Account account = new Account(username, password);
	     AccountModel accmodel= new AccountModel(account);
		 int kq= accmodel.updateAccount();
		 if(kq==0) {
			 message="update that bai";
			 page="Error.jsp";
		 }
		 else {
			 session.setAttribute("accountList", new AccountModel().getList());
			 page="View.jsp";
		 }
		 request.setAttribute("thongbao", message);
		 request.getRequestDispatcher(page).forward(request,response);
	 }
	}

}
